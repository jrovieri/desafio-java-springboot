![Logo Compasso](https://compasso.com.br/wp-content/uploads/2020/07/LogoCompasso-Negativo.png)

# Instruções

A solução foi empacotada com Docker, para executá-la basta clonar o repositório.

```bash
  git clone https://bitbucket.org/jrovieri/desafio-java-springboot
```

E executar o comando
```bash
  docker-compose up
```