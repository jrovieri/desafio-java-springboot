package com.compassouol.desafiojavaspringboot.api;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ProductExceptionHandler.class)
public class ProductControllerTest {

  @Mock ProductService productService;

  @InjectMocks ProductController productController;

  MockMvc mockMvc;

  ProductDTO p0;

  private static final String PRODUCT_ID = "1";
  private static final String PRODUCT_NAME = "Product #1";
  private static final String PRODUCT_DESCRIPTION = "Product #1 Description";
  private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(20.99);

  @BeforeEach
  public void setUp() throws Exception {
    mockMvc =
        MockMvcBuilders.standaloneSetup(productController)
            .setControllerAdvice(ProductExceptionHandler.class)
            .build();

    p0 =
        ProductDTO.builder()
            .id(PRODUCT_ID)
            .name(PRODUCT_NAME)
            .description(PRODUCT_DESCRIPTION)
            .price(PRODUCT_PRICE)
            .build();
  }

  @Test
  public void findById() throws Exception {
    when(productService.findById(anyString())).thenReturn(p0);

    mockMvc
        .perform(get("/products/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", equalTo(PRODUCT_ID)))
        .andExpect(jsonPath("$.name", equalTo(PRODUCT_NAME)));
  }

  @Test
  public void findById_NotFound() throws Exception {
    when(productService.findById(anyString())).thenThrow(ResourceNotFoundException.class);

    mockMvc
        .perform(get("/products/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$").doesNotExist());
  }

  @Test
  public void findAll() throws Exception {
    ProductDTO p1 =
        ProductDTO.builder()
            .id("2")
            .name("Product #2")
            .description("Product #2 Description")
            .price(BigDecimal.valueOf(10.99))
            .build();

    when(productService.findAll()).thenReturn(List.of(p0, p1));

    mockMvc
        .perform(get("/products").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)));
  }

  @Test
  public void findAll_Empty() throws Exception {
    when(productService.findAll()).thenReturn(new ArrayList<>());

    mockMvc
        .perform(get("/products").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  public void update() throws Exception {
    when(productService.update(anyString(), any(ProductDTO.class))).thenReturn(p0);

    mockMvc
        .perform(
            put("/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(p0)))
        .andExpect(status().isOk());
  }

  @Test
  public void update_NotFound() throws Exception {
    when(productService.update(anyString(), any(ProductDTO.class)))
        .thenThrow(ResourceNotFoundException.class);

    mockMvc
        .perform(
            put("/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(p0)))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$").doesNotExist());
  }

  @Test
  public void deleteById() throws Exception {
    mockMvc
        .perform(delete("/products/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
    verify(productService).deleteById(anyString());
  }

  @Test
  public void create() throws Exception {
    when(productService.save(any(ProductDTO.class))).thenReturn(p0);

    mockMvc
        .perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(p0)))
        .andExpect(status().isCreated());
    verify(productService).save(any(ProductDTO.class));
  }

  @Test
  public void createExceptionName() throws Exception {
    ProductDTO invalidProduct =
        ProductDTO.builder().description(PRODUCT_DESCRIPTION).price(PRODUCT_PRICE).build();

    mockMvc
        .perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(invalidProduct)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.status_code", equalTo(400)))
        .andExpect(jsonPath("$.message", equalTo("O campo 'name' deve conter o nome do produto")));

    verifyNoInteractions(productService);
  }

  @Test
  public void createExceptionDescription() throws Exception {
    ProductDTO invalidProduct =
        ProductDTO.builder().name(PRODUCT_NAME).description("").price(PRODUCT_PRICE).build();

    mockMvc
        .perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(invalidProduct)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.status_code", equalTo(400)))
        .andExpect(
            jsonPath(
                "$.message", equalTo("O campo 'description' deve conter a descrição do produto")));

    verifyNoInteractions(productService);
  }

  @Test
  public void createExceptionBlankPrice() throws Exception {
    ProductDTO invalidProduct =
        ProductDTO.builder().name(PRODUCT_NAME).description(PRODUCT_DESCRIPTION).build();

    mockMvc
        .perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(invalidProduct)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.status_code", equalTo(400)))
        .andExpect(
            jsonPath("$.message", equalTo("O campo 'price' deve conter o preço do produto")));

    verifyNoInteractions(productService);
  }

  @Test
  public void createExceptionMinPrice() throws Exception {
    ProductDTO invalidProduct =
        ProductDTO.builder()
            .name(PRODUCT_NAME)
            .description(PRODUCT_DESCRIPTION)
            .price(BigDecimal.valueOf(0.0))
            .build();

    mockMvc
        .perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(invalidProduct)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.status_code", equalTo(400)))
        .andExpect(
            jsonPath("$.message", equalTo("O campo 'price' deve conter um número positivo")));

    verifyNoInteractions(productService);
  }
}
