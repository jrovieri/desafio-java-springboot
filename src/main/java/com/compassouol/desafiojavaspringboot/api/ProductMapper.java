package com.compassouol.desafiojavaspringboot.api;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {

  ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

  Product productDtoToProduct(ProductDTO productDto);

  ProductDTO productToProductDto(Product product);
}
