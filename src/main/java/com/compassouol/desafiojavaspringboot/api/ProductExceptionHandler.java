package com.compassouol.desafiojavaspringboot.api;

import java.util.List;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ProductExceptionHandler {

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  protected ProductErrorMessage handleException(HttpMessageNotReadableException ex) {
    ProductErrorMessage message =
        ProductErrorMessage.builder()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .message("Formato incorreto")
            .build();
    return message;
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  protected ProductErrorMessage handleException(MethodArgumentNotValidException ex) {
    List<FieldError> errors = ex.getBindingResult().getFieldErrors();
    ProductErrorMessage message =
        ProductErrorMessage.builder().statusCode(HttpStatus.BAD_REQUEST.value()).build();

    if (errors.isEmpty()) {
      message.setMessage("Erro de validação");
      return message;
    }

    message.setMessage(errors.get(0).getDefaultMessage());
    return message;
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  protected void handleResourceNotFoundException(ResourceNotFoundException ex) {}
}
