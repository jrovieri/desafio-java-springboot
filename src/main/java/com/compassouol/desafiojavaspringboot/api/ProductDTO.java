package com.compassouol.desafiojavaspringboot.api;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@NoArgsConstructor
public class ProductDTO {

  private String id;

  @NotBlank(message = "O campo 'name' deve conter o nome do produto")
  private String name;

  @NotBlank(message = "O campo 'description' deve conter a descrição do produto")
  private String description;

  @NotNull(message = "O campo 'price' deve conter o preço do produto")
  @DecimalMin(
      value = "0.0",
      message = "O campo 'price' deve conter um número positivo",
      inclusive = false)
  private BigDecimal price;

  @Builder
  public ProductDTO(String id, String name, String description, BigDecimal price) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
  }
}
