package com.compassouol.desafiojavaspringboot.api;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {

  @Autowired private ProductService productService;

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public ProductDTO addProduct(@Valid @RequestBody ProductDTO productDTO) {
    return productService.save(productDTO);
  }

  @PutMapping("/{id}")
  public ProductDTO updateProduct(
      @PathVariable String id, @Valid @RequestBody ProductDTO productDTO) {
    return productService.update(id, productDTO);
  }

  @GetMapping("/{id}")
  public ProductDTO getProductById(@PathVariable String id) {
    return productService.findById(id);
  }

  @GetMapping
  public List<ProductDTO> getAllProducts() {
    return productService.findAll();
  }

  @GetMapping("/search")
  public List<ProductDTO> searchProduct(
      @RequestParam(name = "q", required = false, defaultValue = "") String query,
      @RequestParam(name = "min_price", required = false, defaultValue = "0.0") BigDecimal minPrice,
      @RequestParam(name = "max_price", required = false, defaultValue = "0.0")
          BigDecimal maxPrice) {
    return productService.search(query, minPrice, maxPrice);
  }

  @DeleteMapping("/{id}")
  public void deleteProduct(@PathVariable String id) {
    productService.deleteById(id);
  }
}
