package com.compassouol.desafiojavaspringboot.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

  @PersistenceContext private EntityManager entityManager;

  private ProductMapper productMapper;

  private ProductRepository productRepository;

  @Autowired
  public ProductServiceImpl(
      EntityManager entityManager,
      ProductMapper productMapper,
      ProductRepository productRepository) {
    this.entityManager = entityManager;
    this.productMapper = productMapper;
    this.productRepository = productRepository;
  }

  @Override
  public ProductDTO save(ProductDTO productDto) {
    Product product = productMapper.productDtoToProduct(productDto);
    Product savedProduct = productRepository.save(product);

    return productMapper.productToProductDto(savedProduct);
  }

  @Override
  public ProductDTO update(String id, ProductDTO productDto) {
    if (!productRepository.existsById(Long.valueOf(id))) throw new ResourceNotFoundException();

    productDto.setId(id);
    Product savedProduct = productRepository.save(productMapper.productDtoToProduct(productDto));

    return productMapper.productToProductDto(savedProduct);
  }

  @Override
  public ProductDTO findById(String id) {
    return productRepository
        .findById(Long.valueOf(id))
        .map(productMapper::productToProductDto)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public List<ProductDTO> findAll() {
    return productRepository.findAll().stream()
        .map(productMapper::productToProductDto)
        .collect(Collectors.toList());
  }

  @Override
  public void deleteById(String id) {
    if (!productRepository.existsById(Long.valueOf(id))) throw new ResourceNotFoundException();
    productRepository.deleteById(Long.valueOf(id));
  }

  @Override
  public List<ProductDTO> search(String query, BigDecimal minPrice, BigDecimal maxPrice) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Product> cq = cb.createQuery(Product.class);

    Root<Product> product = cq.from(Product.class);
    List<Predicate> predicates = new ArrayList<>();

    String pattern = "%" + query + "%";
    if (!query.isBlank()) {
      predicates.add(
          cb.or(
              cb.like(product.get("name"), pattern), cb.like(product.get("description"), pattern)));
    }

    if (minPrice.compareTo(BigDecimal.ZERO) == 1) {
      predicates.add(cb.greaterThan(product.get("price"), minPrice));
    }

    if (maxPrice.compareTo(BigDecimal.ZERO) == 1) {
      predicates.add(cb.lessThan(product.get("price"), maxPrice));
    }

    Predicate[] arr = new Predicate[predicates.size()];
    cq.where(predicates.toArray(arr));

    return entityManager.createQuery(cq).getResultList().stream()
        .map(productMapper::productToProductDto)
        .collect(Collectors.toList());
  }
}
