package com.compassouol.desafiojavaspringboot.api;

import java.math.BigDecimal;
import java.util.List;

public interface ProductService {

  ProductDTO save(ProductDTO productDto);

  ProductDTO update(String id, ProductDTO productDto);

  ProductDTO findById(String id);

  List<ProductDTO> findAll();

  List<ProductDTO> search(String query, BigDecimal minPrice, BigDecimal maxPrice);

  void deleteById(String id);
}
