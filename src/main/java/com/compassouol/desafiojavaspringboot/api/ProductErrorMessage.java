package com.compassouol.desafiojavaspringboot.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductErrorMessage {

  @JsonProperty("status_code")
  private int statusCode;

  private String message;
}
